const Express = require("express");

const ctlToken = require("./../controller/token");

const auth = require('./../config/token');

const router = Express.Router();


router.get("/token", auth,ctlToken.generateNewToken);


module.exports = router;
