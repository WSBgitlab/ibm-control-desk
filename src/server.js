const Express = require('express');

const body = require('body-parser');

const cors = require('cors');

const dotenv = require('dotenv');

const routerPOST = require('./routers/routersPOST');

const routerGET = require('./routers/routersGET');

const app = Express();


app.use(function(req,res,next){
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods","GET,POST,PUT,DELETE");
    res.setHeader("Access-Controll-Allow-Headers","content-type");
    res.setHeader("Content-Type","application/json");
    res.setHeader("Access-Controll-Allow-Credentials",true);
    res.setHeader("Accept-Encoding","gzip,deflate");
    res.setHeader("X-Frame-Options","deny");

    next();
});




dotenv.config();

app.use(cors());

app.use(body.urlencoded({ extended : true }));

app.use(Express.json());

app.use(routerPOST);

app.use(routerGET);

app.listen(5959);