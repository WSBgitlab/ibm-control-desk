const jwt = require("jsonwebtoken");


module.exports = async function(req,res,next){
    const token = req.header('token');

    if(!token) return res.status(406).send('Access Danied!');

    try{
        if(token === process.env.TOKEN){
            return res.status(200).send('Token Valido!');
        }else{
            return res.status(406).send('Token Inválido!');
        } 
    }catch(error){
        return res.status(406).send('Invalid Token');
    }
}